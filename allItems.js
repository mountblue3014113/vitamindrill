const items = require("../vitaminDrill/data.js");

const itemsAvailable = (items) => {
  try {
    if (items && items.length > 0) {
      const availableItems = items.filter((item) => item.available);
      console.log(availableItems);
    } else {
      throw new Error("The items array is either empty or does not exist.");
    }
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
};
itemsAvailable(items);
