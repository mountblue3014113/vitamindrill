// 4. Group items based on the Vitamins that they contain
const items = require("./data.js");

const itemsGroupedBasedOnVitamins = (items) => {
  try {
    const groupedItems = {};

    items.forEach((item) => {
      if (!item.contains) {
        throw new Error("property is missing ");
      }

      const vitamins = item.contains
        .split(",")
        .map((vitamin) => vitamin.trim());

      vitamins.forEach((vitamin) => {
        if (!groupedItems[vitamin]) {
          groupedItems[vitamin] = [];
        }
        groupedItems[vitamin].push(item.name);
      });
    });

    console.log(groupedItems);
  } catch (error) {
    console.error(error);
  }
};
itemsGroupedBasedOnVitamins(items);
