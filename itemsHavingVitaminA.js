// 3. Get all items containing Vitamin A.
const items = require("./data.js");

const itemsHavingVitaminA = (items) => {
  try {
    const itemsHavingVitaminA = items.filter((item) => {
      if (!item.contains) {
        throw new Error("missing property !");
      }
      return item.contains.includes("Vitamin A");
    });

    console.log(itemsHavingVitaminA);
  } catch (error) {
    console.error(error);
  }
};
itemsHavingVitaminA(items);
