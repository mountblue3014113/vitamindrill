// 2. Get all items containing only Vitamin C.
const items = require("./data.js");

const itemshavingOnlyVitaminC = (items) => {
    try {
        const itemsContainingVitaminC = items.filter(item => {
            if (!item.contains) {
                throw new Error("property is missing for item.");
            }
            return item.contains === "Vitamin C";
        });
    
        console.log(itemsContainingVitaminC);
    } catch (error) {
        console.error(error);
    }
    
};
itemshavingOnlyVitaminC(items);