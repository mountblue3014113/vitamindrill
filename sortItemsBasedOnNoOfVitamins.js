// 5. Sort items based on number of Vitamins they contain.
const items = require("./data.js");

const sortItemsBasedOnNoOfVitamins = (items) => {
  function countVitamins(item) {
    if (!item.contains) {
      return 0;
    } else {
      return item.contains.split(",").length;
    }
  }
  try {
    items.sort((a, b) => {
      const countA = countVitamins(a);
      const countB = countVitamins(b);

      if (countA > countB) {
        return -1;
      } else if (countA < countB) {
        return 1;
      } else {
        return 0;
      }
    });

    console.log(items);
  } catch (error) {
    console.error(error);
  }
};
sortItemsBasedOnNoOfVitamins(items);
